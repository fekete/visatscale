# visatscale

Course lab for visualization at scale, by Jean-Daniel Fekete (jean-daniel.fekete@inria.fr) and Alejandro Ribes (alejandro.ribes@edf.fr).

# Installing

Install the Anaconda python distribution on your machine. There are two ways to install it: the full distribution and the small distribution. You need to be connected to a fast internet connexion.

## Installing the full Anaconda distribution

If you want to use Python in the future for real, the full distribution contains already loaded all the popular packages so you will not have to download them one by one. The full distribution is large so if you have disk space issues, you may want to use the miniconda distribution.

Visit Anaconda's web site at https://www.anaconda.com/products/individual, download and install installer for the appropriate system (Windows, MacOS, or Linux).

Once it is downloaded and installed, you can open a shell or command window to run python. You need to make sure that the anaconda binary directory is in your PATH.


## Installing the miniconda distribution


The miniconda distribution is available here: https://docs.conda.io/en/latest/miniconda.html

Download it and install it locally. To use it, you need to add the binary directory in your PATH.


## Installing the cours lab files

Once anaconda (full or mini) is installed and usable (the PATH is updated), you can download the course lab material. Connect to a directory where you want to course material loaded and
clone the gitlab directory locally :

``` shell
git clone https://gitlab.inria.fr/fekete/visatscale.git
```

Then `cd visatscale` and install the required python packages by typing in your shell:

``` shell
conda env create -f environment.yml
```

This will create an anaconda environment, and download the required packages if they are not already in anaconda. To use that anaconda environment, you need to type the following command in your shell (every time you resume working on the class project):

``` shell
conda activate visatscale
```

For the course lab, we will use example files from the datashader packages. To download them, type in your shell:

``` shell
datashader examples
```
This will take a bit of time since the files to download are large.

